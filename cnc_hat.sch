EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J1
U 1 1 602AE24F
P 2250 3400
F 0 "J1" H 2300 4517 50  0000 C CNN
F 1 "Conn_02x20_Odd_Even" H 2300 4426 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x20_P2.54mm_Horizontal" H 2250 3400 50  0001 C CNN
F 3 "~" H 2250 3400 50  0001 C CNN
	1    2250 3400
	1    0    0    -1  
$EndComp
Text GLabel 1750 2500 0    50   Input ~ 0
3V3
Text GLabel 2750 2500 2    50   Input ~ 0
5V0
Text GLabel 2750 2600 2    50   Input ~ 0
5V0
Text GLabel 1750 2600 0    50   Input ~ 0
GPIO2(SDA)
Text GLabel 1750 2700 0    50   Input ~ 0
GPIO3(SCL)
Text GLabel 1750 2800 0    50   Input ~ 0
GPIO4(GPCLK0)
Text GLabel 1750 2900 0    50   Input ~ 0
GND
Text GLabel 1750 3000 0    50   Input ~ 0
GPIO17
Text GLabel 1750 3100 0    50   Input ~ 0
GPIO27
Text GLabel 1750 3200 0    50   Input ~ 0
GPIO22
Text GLabel 1750 3300 0    50   Input ~ 0
3V3
Text GLabel 1750 3400 0    50   Input ~ 0
GPIO10(MOSI)
Text GLabel 1750 3500 0    50   Input ~ 0
GPIO9(MISO)
Text GLabel 1750 3600 0    50   Input ~ 0
GPIO11(SCLK)
Text GLabel 1750 3700 0    50   Input ~ 0
GND
Text GLabel 1750 3800 0    50   Input ~ 0
GPIO0(ID_SD)
Text GLabel 1750 3900 0    50   Input ~ 0
GPIO5
Text GLabel 1750 4000 0    50   Input ~ 0
GPIO6
Text GLabel 1750 4100 0    50   Input ~ 0
GPIO13(PWM1)
Text GLabel 1750 4200 0    50   Input ~ 0
GPIO19(PCM_FS)
Text GLabel 1750 4300 0    50   Input ~ 0
GPIO26
Text GLabel 1750 4400 0    50   Input ~ 0
GND
Text GLabel 2750 2700 2    50   Input ~ 0
GND
Text GLabel 2750 2800 2    50   Input ~ 0
GPIO14(TXD)
Text GLabel 2750 2900 2    50   Input ~ 0
GPIO15(RXD)
Text GLabel 2750 3000 2    50   Input ~ 0
GPIO18(PCM_CLK)
Text GLabel 2750 3100 2    50   Input ~ 0
GND
Text GLabel 2750 3200 2    50   Input ~ 0
GPIO23
Text GLabel 2750 3300 2    50   Input ~ 0
GPIO24
Text GLabel 2750 3400 2    50   Input ~ 0
GND
Text GLabel 2750 3500 2    50   Input ~ 0
GPIO25
Text GLabel 2750 3600 2    50   Input ~ 0
GPIO8(CE0)
Text GLabel 2750 3700 2    50   Input ~ 0
GPIO7(CE1)
Text GLabel 2750 3800 2    50   Input ~ 0
GPIO1(ID_SC)
Text GLabel 2750 3900 2    50   Input ~ 0
GND
Text GLabel 2750 4000 2    50   Input ~ 0
GPIO12(PWM0)
Text GLabel 2750 4100 2    50   Input ~ 0
GND
Text GLabel 2750 4200 2    50   Input ~ 0
GPIO16
Text GLabel 2750 4300 2    50   Input ~ 0
GPIO20(PCM_DIN)
Text GLabel 2750 4400 2    50   Input ~ 0
GPIO21(PCM_DOUT)
Wire Wire Line
	2550 2500 2750 2500
Wire Wire Line
	2750 2600 2550 2600
Wire Wire Line
	2750 2700 2550 2700
Wire Wire Line
	2750 2800 2550 2800
Wire Wire Line
	2750 2900 2550 2900
Wire Wire Line
	2750 3000 2550 3000
Wire Wire Line
	2750 3100 2550 3100
Wire Wire Line
	2750 3200 2550 3200
Wire Wire Line
	2550 3300 2750 3300
Wire Wire Line
	2550 3400 2750 3400
Wire Wire Line
	2550 3500 2750 3500
Wire Wire Line
	2550 3600 2750 3600
Wire Wire Line
	2550 3700 2750 3700
Wire Wire Line
	2550 3800 2750 3800
Wire Wire Line
	2550 3900 2750 3900
Wire Wire Line
	2550 4000 2750 4000
Wire Wire Line
	2550 4100 2750 4100
Wire Wire Line
	2550 4200 2750 4200
Wire Wire Line
	2550 4300 2750 4300
Wire Wire Line
	2550 4400 2750 4400
Wire Wire Line
	2050 4400 1750 4400
Wire Wire Line
	2050 4300 1750 4300
Wire Wire Line
	2050 4200 1750 4200
Wire Wire Line
	2050 4100 1750 4100
Wire Wire Line
	2050 4000 1750 4000
Wire Wire Line
	2050 3900 1750 3900
Wire Wire Line
	2050 3800 1750 3800
Wire Wire Line
	2050 3700 1750 3700
Wire Wire Line
	2050 3600 1750 3600
Wire Wire Line
	2050 3500 1750 3500
Wire Wire Line
	2050 3400 1750 3400
Wire Wire Line
	2050 3300 1750 3300
Wire Wire Line
	2050 3200 1750 3200
Wire Wire Line
	2050 3100 1750 3100
Wire Wire Line
	2050 3000 1750 3000
Wire Wire Line
	2050 2900 1750 2900
Wire Wire Line
	2050 2800 1750 2800
Wire Wire Line
	2050 2700 1750 2700
Wire Wire Line
	2050 2600 1750 2600
Wire Wire Line
	2050 2500 1750 2500
$Sheet
S 8250 5950 1600 350 
U 604E1E00
F0 "INPUTS" 50
F1 "file604E1DFF.sch" 50
$EndSheet
$Sheet
S 8250 5350 1600 400 
U 6052759F
F0 "POWER" 50
F1 "file6052759E.sch" 50
$EndSheet
$Sheet
S 6500 5950 1600 350 
U 6055837D
F0 "OUTPUTS" 50
F1 "file6055837C.sch" 50
$EndSheet
$Sheet
S 6500 5350 1600 400 
U 60650143
F0 "CAN_BUS" 50
F1 "file60650142.sch" 50
$EndSheet
$Sheet
S 5600 5950 500  350 
U 60659FD8
F0 "RS485" 50
F1 "file60659FD7.sch" 50
$EndSheet
$EndSCHEMATC
