
(function($){

	$.widget('itti.productautocomplete', {

		options : {
			url : '',
			lang : '',
			supplier_code:''
		},

		_create : function(){
			var self = this;
			this.container = $('<div class="autocomplete-container" />').appendTo($('body'));
			this.previousVal = '';
			this.element.attr('autocomplete', 'off').on('keyup', function(){ self._onKeyUp(); });
			this.container.on('autocomplete', function(event, data){
				self.element.val(data.val + ' ').focus();
				self.container.hide();
			});
			this.element.on('blur', function(){
				setTimeout(function(){
					self.container.hide();
				}, 1000);
			});
		},

		_displayContent : function(content){
			if (content==='') {
				this.container.hide();
			} else {
				var offset = this.element.offset();
				offset.top += this.element.outerHeight(true);
				offset.left += 'px';
				offset.top += 'px';
				offset['min-width'] = this.element.width() + "px";
				this.container.empty().append(content).css(offset).show();
			}
		},

		_showHint : function(){
			var self = this;
			this._getHint(function(response){
				self._displayContent(response);
			});
		},

		_getHint : function(callback){
			// show container with a loading overlay
			var overlay = $('<div class="loading_overlay"><img src="/i/ajax-loader.gif" /></div>');
			overlay.show().css({width:this.element.width()+'px'});
			this._displayContent(overlay);

			// stop any previous request
			if (typeof(this.ajaxPromise)!=='undefined') {
				this.ajaxPromise.abort();
			}
			this.ajaxPromise = $.get(
				this.options.url,
				{
					lang:this.options.lang,
					keywords:this.element.val(),
					supplier_code:this.options.supplier_code
				},
				callback,
				'html');
		},

		_onKeyUp : function(){
			if (this.element.val()===this.previousVal) {
				return;
			}

			this.previousVal = this.element.val();

			var self = this;
			clearTimeout(this._timeout);
			this._timeout = setTimeout(function(){ self._showHint(); }, 200);
		}

	});

})(jQuery);
