$.widget('itti.dropdownOpener', {
	options : {
		"classOpened" : "open",
		"classClosed" : "closed",
		"openOn" : "click", // or hover
		"menuZIndex" : 1
	},
	_create : function(){
		var menu = $("[data-dropdown-menu='"+this.element.attr('data-dropdown-opener')+"']").first();
		if (!menu.length) {
			return;
		}
		this.menu = menu;

		this.menu.css('z-index', this.options.menuZIndex);

		var self = this;

		this.options = $.extend(this.options, this.element.data('dropdownOptions'));

		switch (this.options.openOn) {
			case 'click':
				this.element.on('click', function (e) {
					e.preventDefault();
					if (self.element.is('.' + self.options.classOpened)) {
						self.close();
					} else {
						self.open();
					}
				});
				break;
			case 'hover':
				this.element.on('mouseenter', this.open.bind(this));
				this.menu.on('mouseleave', this.close.bind(this));
				break;
			default:
				console.warn('Unknown openOn: '+this.options.openOn);
		}

		this._makeOverlay().insertBefore(this.menu).on('click', function(){
			self.close();
		});

		this.close();
	},
	_makeOverlay : function(){
		return this.overlay = $('<div/>')
			.css({
				"position": "fixed",
				"width": "100%",
				"height": "100%",
				"left": "0",
				"top": "0",
				"z-index": this.options.menuZIndex
			})
			;
	},
	open : function(){
		this.overlay.show();
		this.element.removeClass(this.options.classClosed);
		this.menu.removeClass(this.options.classClosed);
		this.element.addClass(this.options.classOpened);
		this.menu.addClass(this.options.classOpened);
	},
	close : function(){
		this.overlay.hide();
		this.element.removeClass(this.options.classOpened);
		this.menu.removeClass(this.options.classOpened);
		this.element.addClass(this.options.classClosed);
		this.menu.addClass(this.options.classClosed);
	}
});