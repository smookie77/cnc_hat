function changeBtn(btn, curr) {
	var myTextcurrency=curr;
	//$('select[name="currency"]').val(myTextcurrency);
	ITTI.sessionSettings.set('currency', curr , {expires:null});
	window.location.reload();
}

window.translation = {
	pagebar_splitter : '{#Product list - Pagebar splitter#}',
	product_comparison_needs_2 : '{#Cannot compare less than 2 products#}',
	order_not_enough_stock : '{#Shopping cart error - Not enough stock#}'
};

function change_language(lang) {
	var path = window.location.pathname;

	if (window.lng_current !== window.lng_default) {
		path = path.replace(new RegExp('^\/'+window.lng_current), '');
	}

	if (lang !== window.lng_default) {
		path = '/' + lang + path;
	}

	var l = window.location;

	window.location.replace(l.protocol+'//'+l.host+path+l.search+(l.hash==='#' ? '' : l.hash));
}

jQuery(function($){

	// dropdown menu
	$('[data-dropdown-opener]').dropdownOpener();

	window.shoppingCart = new ITTI.shoppingCart();
	window.shoppingCart.initContent(document.body);

	$('.rounded_box').each(function(){
		var el = $(this);
		el.html(
			'<div class="rb_top_left"><div class="rb_top_right">'+
			'<div class="rb_bottom_left"><div class="rb_bottom_right">'+
			el.html()+'</div></div></div></div>');
	});

	// set up a timer to detect when the page has been retrieved from the browser history

	var callback = (function(){
		var last_tick = (new Date()).getTime();
		var timeout = 5000;

		return function(){ // that's the callback, the rest is just variable scope
			var new_tick = (new Date()).getTime();

			if ((new_tick - last_tick) > timeout) {
				// update all shopping cart indicators
				window.shoppingCart.refreshCartIndicators();
			}

			last_tick = new_tick;
		};
	})();

	setInterval(callback, 100);

	// create product galleries
	jQuery('.product-detail-view .gallery').productGallery();

	if (jQuery('#header_login_box input[name="username"]').val()=='') {
		jQuery('#header_login_box input[name="username"]').emptyInputText('{#User profile - ID#}', {
			color:'#8C7E72',
			padding: '3px'
		});
	}
	if (jQuery('#header_login_box input[name="password"]').val()=='') {
		jQuery('#header_login_box input[name="password"]').emptyInputText('{#User profile - Password#}', {
			color:'#8C7E72',
			padding: '3px'
		});
	}

	// dynamically load farnell prices

	var window_resize_timeout; // firefox floods the handler with scroll events during smooth scrolling
	$(window).on('resize scroll', function(){
		clearTimeout(window_resize_timeout);
		window_resize_timeout = setTimeout(function(){
			var pending = $('.farnell-prices-missing');
			if (pending.size()==0) return;
			// update css classes to avoid double invocation
			pending.removeClass('farnell-prices-missing').addClass('farnell-prices-loading');
			// add loading overlay
			pending.find('.loading_overlay').remove().end().prepend(
				$('<div class="loading_overlay"><img src="/i/ajax-loader.gif" /></div>')
			);
			// collect product ids in array
			var ids = [];
			pending.each(function(){
				ids.push($(this).data('farnell-id'));
			});
			// get prices via ajax
			$.getJSON('/ajax/farnell_get_prices.php', {language:window.lng_current,ids:ids}, function(response){
				pending.each(function(){
					var id = $(this).data('farnell-id');
					var visible = $(this).data('visible-price');
					if (response[id].prices!=undefined) {
						//if (response[visible]!=undefined) {
						//$(this).closest('tr').find('.order_call_btn').each(function(){
						//	$(this).parent().find('.order_btn').show();
						//	$(this).remove();
						//});
						//} else {
						//
						//}
						$(this).empty().removeClass('farnell-prices-loading').append(response[id].prices);
						$(this).closest('tr').find('.tdstock_show'+id).each(function(){
							$('.tdstock_show'+id).empty().append(response[id].stock);
						});
						//$(this).closest('tr').find('#order'+id).each(function(){
						//	$('#order'+id).empty().append(response[id].order_btn);
						//});
						pending = pending.not(this);
					}
				});
				// if we caused any changes in the flow of elements which brought new prices into view, repeat...
				$(window).triggerHandler('resize');
			});
		}, 200); // wait for 200 ms without any events
	}).triggerHandler('resize');

	// admin logon
	$('a.admin_logon').click(function(){
		var hidden = $('input[name="admin_logon"]');
		var form = hidden.closest('form');
		var val = prompt('Enter client email or username:');

		if (val!=null) {
			hidden.val(val);
			form.attr('action', form.attr('action') + base64_encode(decodeURIComponent(window.location.pathname + window.location.search + window.location.hash)));
			form.get(0).submit();
		}

		return false;
	});

	// DATE/TIME PICKERS

	$.datepicker.setDefaults({dateFormat:'dd.mm.yy',changeYear:true,changeMonth:true,yearRange:'1900:+10',showTimepicker:false});

	$('input[type="ittidate"]').each(function(i,domNode){
		var d=$(domNode);

		var options = d.data('datepicker') || {};
		options.showTimepicker = false;

		var val = d.val();

		if (val) {
			val = Date.parseISO(val);
		}

		if (val) {
			d.val('').datepicker(options).datepicker('setDate', val);
		} else {
			d.datepicker(options);
		}

	});

	$('form').on('submit', function(){

		$(this).find('input[type="ittidate"]').each(function (i, domNode) {
			var d = $(domNode);
			var d2 = d.datepicker('getDate');
			var val = d.val();
			if (d2) {
				d.datepicker('setDate', d2);
				d.val(val == d.val() ? d2.toISODate() : val);
			}
		});

		return true;
	});

	$(".param_filter_container + .param-filter-collapse").each(function(){
		var params = $(this).prev('.param_filter_container');
		var handle = $(this);
		var onresize = function(){
			if (handle.hasClass('collapse-expanded')) return;
			var box_first = params.find('>.param_filter_box').first().position().top;
			var box_last = params.find('>.param_filter_box').last().position().top;
			if (box_first !== box_last) {
				params.add(handle).removeClass('collapse-disabled');
			}
			else {
				params.add(handle).addClass('collapse-disabled');
			}

			var max_left = -1;
			params.find('>.param_filter_box').each(function(){
				var pos = $(this).position().left + $(this).outerWidth();
				if (pos > max_left) {
					max_left = pos;
				}
				else {
					return false;
				}
			});
			if (max_left > 0) {
				handle.css('width', max_left + 'px');
			}
			else {
				handle.css('width', '');
			}
		};
		onresize();
		handle.on('click', function(){
			params.add(handle).toggleClass('collapse-expanded');
		});
		$(window).on('resize', onresize);
	});

});


$(document).mouseup(function (e)
{
	var myDiv = $(".popupprice");
	if (myDiv.has(e.target).length === 0)
		myDiv.hide();
});
$(document).scroll(function (e)
{
	var myDiv = $(".popupprice");
	if (myDiv.has(e.target).length === 0)
		myDiv.hide();
});

function pricePopup(prtable,id_f) {
	var tooltipX2 = posX;
	var tooltipY2 = posY;
	tooltipY2 = tooltipY2 +15;
	ppp = $('#'+id_f).position();
	pppY = $('#'+id_f).scrollTop();
	tooltipY2 = pppY;
	//tooltipX2 = ppp.left;
	var x = (document.pageXOffset?document.pageXOffset:document.body.scrollLeft);
	var y = $('#'+id_f).offset().top - $(window).scrollTop() +22;

	$('.popupprice').css({top: y, left: tooltipX2});
	var code_div=document.getElementById('prcodeprice');
	code_div.innerHTML=prtable;
	$('.popupprice').fadeIn(50);
	$('input').attr('autocomplete', 'off');
}

function requestPopup(prcode1,prname1,qty_min,offer) {

	var height2 = $(document).height();
	var width2 = $(document).width();
	var spanHeight = $('.popup_request').height(); //393
	var spanWidth = $('.popup_request').width(); //370

	var myWidth = 0, myHeight = 0;
	if( typeof( window.innerWidth ) == 'number' ) {
		//Non-IE
		myWidth = window.innerWidth;
		myHeight = window.innerHeight;
	} else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
		//IE 6+ in 'standards compliant mode'
		myWidth = document.documentElement.clientWidth;
		myHeight = document.documentElement.clientHeight;
	} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
		//IE 4 compatible
		myWidth = document.body.clientWidth;
		myHeight = document.body.clientHeight;
	}

	var topel = posY;
	var leftel = posX-350;

	if (leftel-spanWidth<0) leftel=myWidth/2-80;
	if ((height2-posY)<=myHeight/2) {
		topel=posY-200;
	}

	$('.popup_request').css({top: topel, left: leftel});

	var code_div=document.getElementById('prcode1');
	code_div.innerHTML=prcode1;

	var name_div=document.getElementById('prname1');
	name_div.innerHTML=prname1;

	if (offer>0) {
		offer_id=offer;
	} else {
		offer_id = 0;
	}
	var qty_min_div=document.getElementById('qty_min');
	if (qty_min>0) {
		qty_min_htm='<td style=\"border:0;background-color:white;\">{#Min Order Qty #}: </td><td style=\"border:0;background-color:white;font-color:red;\">'+qty_min+'</td>';
	} else {
		qty_min_htm="<td style=\"border:0;background-color:white;\"></td><td style=\"border:0;background-color:white;\"></td>";
	}
	qty_min_div.innerHTML=qty_min_htm;

	document.getElementById('pr_name').value=prname1;
	document.getElementById('pr_code').value=prcode1;
	document.getElementById('qty_min').value=qty_min_htm;
	document.getElementById('pr_offer').value=offer_id;
	document.getElementById('qty').value='';
	document.getElementById('deliveryterm').value='';
	document.getElementById('addinfo').value='';

	var cvr = document.getElementById("cover");
	$('.popup_request').fadeIn(50);
	$("#cover").fadeTo(50, 0.5).css('display','block');
	//cvr.style.display = "block";
	if (document.body.style.overflow = "scroll") {
		cvr.style.width = "100%";
		cvr.style.height = "100%";
	}

}
function requestPopup_temp() {

	var height = $(document).height();
	var width = $(document).width();
	var spanHeight = $('.popup_timetable').height();
	var spanWidth = 500;

	var topel = posY;
	var leftel = posX;

	$('.popup_timetable').css({top: topel, left: leftel});


	$('.popup_timetable').fadeIn(50);
	cvr.style.display = "block";
	if (document.body.style.overflow = "scroll") {
		cvr.style.width = "100%";
		cvr.style.height = "100%";
	}

}

function closePopup() {
	var cvr = document.getElementById("cover");
	cvr.style.display = "none";
	$('.popup_request').fadeOut(100);
}
function closePopup_timetable() {
	$('.popup_timetable').fadeOut(100);
}

//window.onload = init;
document.onmousemove = getCursorXY;

function init() {
	if (window.Event) {
		document.captureEvents(Event.MOUSEMOVE);
	}
	document.onclick = getCursorXY;
}

function getCursorXY(e) {
	if (!e) e = window.event;
	posX = (window.Event) ? e.pageX : e.clientX + (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft);
	posY = (window.Event) ? e.pageY : e.clientY + (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);
	if ((e.clientX || e.clientY) && document.body && document.body.scrollLeft!=null) {
		clickX = e.clientX + document.body.scrollLeft;
		clickY = e.clientY + document.body.scrollTop;
	}
	if ((e.clientX || e.clientY) && document.compatMode==='CSS1Compat' && document.documentElement && document.documentElement.scrollLeft!=null) {
		clickX = e.clientX + document.documentElement.scrollLeft;
		clickY = e.clientY + document.documentElement.scrollTop;
	}
	if (e.pageX || e.pageY) {
		clickX = e.pageX;
		clickY = e.pageY;
	}
	return true;
}

$(function () {
	$('.cookiebar-close').click(function () {
		$('.ui-widget').hide();
		localStorage.setItem('Privacy_policy', 1);
	});

	if(localStorage.getItem('Privacy_policy') == 1){
		$('.ui-widget').hide();
	}
});
