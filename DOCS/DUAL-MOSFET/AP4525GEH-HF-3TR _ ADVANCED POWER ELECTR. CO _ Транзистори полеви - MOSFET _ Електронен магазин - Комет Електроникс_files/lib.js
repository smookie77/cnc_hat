
Date.parseISO = function(val){
	var toint = function(string){
		string = string.replace(/^0*/, '');
		return parseInt(string);
	};
	if (typeof val === 'undefined' || val==='' || val==='0000-00-00' || val==='0000-00-00 00:00:00') return null;
	var match = val.match(/^(\d{4})-(\d{2})-(\d{2})( (\d{2}):(\d{2}):(\d{2}))?$/);
	if (!match) return null;
	if (match[4]) {
		return new Date(toint(match[1]), toint(match[2])-1, toint(match[3]), match[5], match[6], match[7]);
	} else {
		return new Date(toint(match[1]), toint(match[2])-1, toint(match[3]));
	}
};

Number.prototype.padLeft = function(length, padstring){
	var result = this.toString();
	while (result.length<length) {
		result = padstring + result;
	}
	return result;
};

Date.prototype.toISODate = function(){
	return [this.getFullYear().padLeft(4, '0'), (this.getMonth()+1).padLeft(2, '0'), this.getDate().padLeft(2, '0')].join('-');
};

Date.prototype.toISODateTime = function(){
	return this.toISODate() + ' ' + [this.getHours().padLeft(2, '0'), this.getMinutes().padLeft(2, '0'), this.getSeconds().padLeft(2, '0')].join(':');
};

(function($){

	// goes up the parent tree to find out the absolute position of the element (relative to the document)
	$.fn.getAbsolutePosition = function() {
		var curleft = curtop = 0;
		var elem = this.get(0);

		do {
			curleft += elem.offsetLeft;
			curtop += elem.offsetTop;
		} while (elem = elem.offsetParent);

		return {
			left:curleft,
			top:curtop
		};
	}

	$.fn.flyTo = function(destination) {
		return this.each(function() {
			var position_start = $(this).getAbsolutePosition();
			var position_end = $(destination).getAbsolutePosition();
			var clone = $(this).clone(false).appendTo(document.body);

			var anim_start = {
				position : 'absolute',
				top : position_start.top,
				left : position_start.left,
				opacity : 1
			};
			var anim_end = {
				top : position_end.top,
				left : position_end.left,
				opacity : 0
			};

			clone.css(anim_start).animate(anim_end, {
				duration : 1000,
				easing : 'easeInOutQuad',
				complete : function(){ $(this).remove(); }
			});
		});
	};

	$.fn.emptyInputText = function(text, css) {
		return this.each(function(){
			var text_elem = $('<div class="empty_input_text">'+text+'</div>');
			var input = $(this);

			input.wrap('<div />').after(text_elem).focus(function(event){
				text_elem.hide();
			}).blur(function(event){
				if (input.val()=='') text_elem.show();
			});

			text_elem.parent().css('position', 'relative');

			text_elem.css(css).css({
				'cursor' : 'text',
				'z-index' : '1',
				'position' : 'absolute',
				'top' : this.offsetTop+'px',
				'left' : this.offsetLeft+'px',
				'height' : input.height()+'px',
				'width' : input.width()+'px'
			}).click(function(){
				input.focus();
			});
		});
	};

	var gallery_methods = {
		goLeftCallback : function (event) {
			event.preventDefault();
			var self = event.data.self;

			if (self.data('current')==1) {
				self.data('current', self.data('total'));
			} else {
				self.data('current', self.data('current') - 1);
			}

			jQuery('img', self).hide().eq(self.data('current')-1).show();
			jQuery('.indicator', self).get(0).innerHTML = self.data('current') + '/' + self.data('total');

			return false;
		},
		goRightCallback : function (event) {
			event.preventDefault();
			var self = event.data.self;

			if (self.data('current')==self.data('total')) {
				self.data('current', 1);
			} else {
				self.data('current', self.data('current') + 1);
			}

			jQuery('img', self).hide().eq(self.data('current')-1).show();
			jQuery('.indicator', self).get(0).innerHTML = self.data('current') + '/' + self.data('total');

			return false;
		},
		imageClickCallback : function (event) {
			//alert('a');
		}
	};

	$.fn.productGallery = function() {
		return this.each(function() { // iterate over each matched element (they are separate galleries)
			var imgs = jQuery('img', this);

			$(this).data('total', imgs.size());
			$(this).data('current', 1);

			imgs.click(gallery_methods.imageClickCallback).first().show();

			if (imgs.size()>1) {
				jQuery(this).append('<div class="control_buttons"><a href="" class="go_left"></a><a href="" class="go_right"></a><span class="indicator">1/'+imgs.size()+'</span></div>');

				jQuery('a.go_left',  this).click({self:$(this)}, gallery_methods.goLeftCallback);
				jQuery('a.go_right', this).click({self:$(this)}, gallery_methods.goRightCallback);
			}

		});
	};

})(jQuery);



// minifier: http://dean.edwards.name/packer/

function getParentFormElement(elem) {
	form_obj = elem;
	while (form_obj.tagName!='FORM') {
		form_obj = form_obj.parentNode;
		if (!form_obj) {
			alert('Form not found! Please put the list control in a form!'); return 0;
		}
	}
	return form_obj;
}

function getForm(elem) {
	return getParentFormElement(elem);
}

function base64_encode (data) {
	// http://kevin.vanzonneveld.net
	// +   original by: Tyler Akins (http://rumkin.com)
	// +   improved by: Bayron Guevara
	// +   improved by: Thunder.m
	// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   bugfixed by: Pellentesque Malesuada
	// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   improved by: Rafał Kukawski (http://kukawski.pl)
	// -    depends on: utf8_encode
	// *     example 1: base64_encode('Kevin van Zonneveld');
	// *     returns 1: 'S2V2aW4gdmFuIFpvbm5ldmVsZA=='
	// mozilla has this native
	// - but breaks in 2.0.0.12!
	//if (typeof this.window['atob'] == 'function') {
	//    return atob(data);
	//}
	var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
	var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
			ac = 0,
			enc = "",
			tmp_arr = [];

	if (!data) {
			return data;
	}

	data = utf8_encode(data + '');

	do { // pack three octets into four hexets
			o1 = data.charCodeAt(i++);
			o2 = data.charCodeAt(i++);
			o3 = data.charCodeAt(i++);

			bits = o1 << 16 | o2 << 8 | o3;

			h1 = bits >> 18 & 0x3f;
			h2 = bits >> 12 & 0x3f;
			h3 = bits >> 6 & 0x3f;
			h4 = bits & 0x3f;

			// use hexets to index into b64, and append result to encoded string
			tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
	} while (i < data.length);

	enc = tmp_arr.join('');

	var r = data.length % 3;

	return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);

}

function base64_decode (data) {
	// http://kevin.vanzonneveld.net
	// +   original by: Tyler Akins (http://rumkin.com)
	// +   improved by: Thunder.m
	// +      input by: Aman Gupta
	// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   bugfixed by: Onno Marsman
	// +   bugfixed by: Pellentesque Malesuada
	// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +      input by: Brett Zamir (http://brett-zamir.me)
	// +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// -    depends on: utf8_decode
	// *     example 1: base64_decode('S2V2aW4gdmFuIFpvbm5ldmVsZA==');
	// *     returns 1: 'Kevin van Zonneveld'
	// mozilla has this native
	// - but breaks in 2.0.0.12!
	//if (typeof this.window['btoa'] == 'function') {
	//    return btoa(data);
	//}
	var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
	var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
			ac = 0,
			dec = "",
			tmp_arr = [];

	if (!data) {
			return data;
	}

	data += '';

	do { // unpack four hexets into three octets using index points in b64
			h1 = b64.indexOf(data.charAt(i++));
			h2 = b64.indexOf(data.charAt(i++));
			h3 = b64.indexOf(data.charAt(i++));
			h4 = b64.indexOf(data.charAt(i++));

			bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;

			o1 = bits >> 16 & 0xff;
			o2 = bits >> 8 & 0xff;
			o3 = bits & 0xff;

			if (h3 == 64) {
					tmp_arr[ac++] = String.fromCharCode(o1);
			} else if (h4 == 64) {
					tmp_arr[ac++] = String.fromCharCode(o1, o2);
			} else {
					tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
			}
	} while (i < data.length);

	dec = tmp_arr.join('');
	dec = utf8_decode(dec);

	return dec;
}

function utf8_encode (argString) {
	// http://kevin.vanzonneveld.net
	// +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
	// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   improved by: sowberry
	// +    tweaked by: Jack
	// +   bugfixed by: Onno Marsman
	// +   improved by: Yves Sucaet
	// +   bugfixed by: Onno Marsman
	// +   bugfixed by: Ulrich
	// +   bugfixed by: Rafal Kukawski
	// *     example 1: utf8_encode('Kevin van Zonneveld');
	// *     returns 1: 'Kevin van Zonneveld'

	if (argString === null || typeof argString === "undefined") {
			return "";
	}

	var string = (argString + ''); // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");
	var utftext = "",
			start, end, stringl = 0;

	start = end = 0;
	stringl = string.length;
	for (var n = 0; n < stringl; n++) {
			var c1 = string.charCodeAt(n);
			var enc = null;

			if (c1 < 128) {
					end++;
			} else if (c1 > 127 && c1 < 2048) {
					enc = String.fromCharCode((c1 >> 6) | 192) + String.fromCharCode((c1 & 63) | 128);
			} else {
					enc = String.fromCharCode((c1 >> 12) | 224) + String.fromCharCode(((c1 >> 6) & 63) | 128) + String.fromCharCode((c1 & 63) | 128);
			}
			if (enc !== null) {
					if (end > start) {
							utftext += string.slice(start, end);
					}
					utftext += enc;
					start = end = n + 1;
			}
	}

	if (end > start) {
			utftext += string.slice(start, stringl);
	}

	return utftext;
}

function utf8_decode (str_data) {
	// http://kevin.vanzonneveld.net
	// +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
	// +      input by: Aman Gupta
	// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   improved by: Norman "zEh" Fuchs
	// +   bugfixed by: hitwork
	// +   bugfixed by: Onno Marsman
	// +      input by: Brett Zamir (http://brett-zamir.me)
	// +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// *     example 1: utf8_decode('Kevin van Zonneveld');
	// *     returns 1: 'Kevin van Zonneveld'
	var tmp_arr = [],
			i = 0,
			ac = 0,
			c1 = 0,
			c2 = 0,
			c3 = 0;

	str_data += '';

	while (i < str_data.length) {
			c1 = str_data.charCodeAt(i);
			if (c1 < 128) {
					tmp_arr[ac++] = String.fromCharCode(c1);
					i++;
			} else if (c1 > 191 && c1 < 224) {
					c2 = str_data.charCodeAt(i + 1);
					tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
					i += 2;
			} else {
					c2 = str_data.charCodeAt(i + 1);
					c3 = str_data.charCodeAt(i + 2);
					tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
					i += 3;
			}
	}

	return tmp_arr.join('');
}


ITTI = {

	pack : function(object) {
		return base64_encode(json_encode(object));
	},

	unpack : function(string) {
		return json_decode(base64_decode(string));
	},

	searchform : function (outer_container) {

		if (!(this instanceof arguments.callee)) {
			throw new Error("Constructor called as a static function");
		}

		this.log_stats = false;

		this.containers = {
			main : outer_container,
			containing_form : jQuery(outer_container).closest('form'),
			param_search : jQuery('.param_filter_container', outer_container),
			param_search_overlay : jQuery('.param_filter_container .loading_overlay', outer_container),
			compare_counter : jQuery('span.comparison_count', outer_container),
			pagebar : jQuery('.search_result_page_links', outer_container),
			product_listing : jQuery('div.search_result_list .content', outer_container),
			product_listing_overlay : jQuery('div.search_result_list .loading_overlay', outer_container).detach(),
			empty_result : jQuery('div.search_result_list .empty_content', outer_container),
			third_level_category_data : jQuery('.third_level_category_data_show', outer_container),
			request_form : jQuery('div.search_result_list .request_form', outer_container),
			category_listing : jQuery('.search_result_categories', outer_container),

			checkboxes_by_id : {},
			counters_by_id : {},
			labels_by_id : {}
		};

		// you can use "itti_self" in inline functions defined in the current scope
		var itti_self = this;

		this.comparison = {
			// all methods of this object are called in the context of the parent object
			// by calling them like this: method.call(itti_self)

			product_ids : [],

			addProduct : function (pid) {
				for (var i in this.comparison.product_ids) {
					if (this.comparison.product_ids[i]==pid) return true;
				}

				this.comparison.product_ids.push(pid);
				this.comparison.updateCounter.call(this);

				return true;
			},

			removeProduct : function (pid) {
				var c = this.comparison;

				for (var i in c.product_ids) {
					if (c.product_ids[i]==pid) {
						c.product_ids.splice(i, 1);
						c.updateCounter.call(this);
						return true;
					}
				}
			},

			updateCheckboxes : function () {
				jQuery('input[name="compare[]"]', this.containers.product_listing).each(function(){
					var a = itti_self.comparison.product_ids;
					var v = parseInt(this.value);

					this.checked = false;

					for (var i in a) {
						if (a[i]==v) {
							this.checked = true;
						}
					}
				});

				this.comparison.updateCounter.call(this);
			},

			updateCounter : function () {
				if (this.containers.compare_counter.size()==0) return 0;
				var t = this;
				this.containers.compare_counter.each(function(){
					this.innerHTML = '('+t.comparison.product_ids.length+')';
				});
			},

			restoreClickHandlers : function () {
				jQuery('input[name="compare[]"]', this.containers.product_listing)
				.click(this.comparison.onCheckboxClickHandler);
			},

			// called in the context of a checkbox element
			onCheckboxClickHandler : function (event) {
				if (this.checked) {
					if (!itti_self.comparison.addProduct.call(itti_self, parseInt(this.value))) {
						event.preventDefault();
					}
				} else {
					itti_self.comparison.removeProduct.call(itti_self, parseInt(this.value));
				}
			},

			doCompare : function () {
				if (this.comparison.product_ids.length<2) {
					return false;
				}

				var ids = this.comparison.product_ids.join(',');

				window.location.href = window.product_comparison_url + ids;

				return true;
			}

		}; // this.comparison END

		jQuery('input[type=checkbox]', this.containers.param_search).each(function(index,element){
			itti_self.containers.checkboxes_by_id[element.value] = element;
			itti_self.containers.counters_by_id[element.value] = document.getElementById('cnt'+element.value);
			itti_self.containers.labels_by_id[element.value] = jQuery(element).closest('label').get(0);
		});

		this.sync_listing = true;


		// PREVENT FORM SUBMIT
		this.containers.containing_form.submit(function(event){
			event.preventDefault();
			itti_self.startUpdatingResultCounters();
			return false;
		});


		// CHECKBOX HANDLERS

		this.checkboxClickHandler = function (event) {
			// clear the range selectors
			if(window.location.href.indexOf("/CatalogueFarnell/") > -1) {
				ga('send','event','Params','Click','CatalogFarnell');
			} else {
				ga('send','event','Params','Click','Catalog');
			}
			itti_self.clearMinMaxSelectors(jQuery(this).closest('.param_filter_box'));
			if (event.target.checked) {
				$(itti_self.containers.labels_by_id[event.target.value]).addClass('cb_checked');
			} else {
				$(itti_self.containers.labels_by_id[event.target.value]).removeClass('cb_checked');
			}
			itti_self.startUpdatingResultCounters();
		};

		for (var val in this.containers.checkboxes_by_id) {
			jQuery(this.containers.checkboxes_by_id[val]).click(this.checkboxClickHandler);
		}


		// MASS SELECT HANDLERS

		// receives jQuery object
		this.cbSetChecked = function (collection, set_checked) {
			collection.each(function(index, element){
				if (set_checked) {
					element.checked = true;
					$(itti_self.containers.labels_by_id[element.value]).addClass('cb_checked');
				} else {
					element.checked = false;
					$(itti_self.containers.labels_by_id[element.value]).removeClass('cb_checked');
				}
			});
		};

		// receives jQuery object
		this.cbSetDisabled = function (collection, set_disabled) {
			collection.each(function(index, element){
				if (set_disabled) {
					jQuery(element)
					.attr('disabled', 'disabled')
					.closest('label')
					.addClass('cb_disabled');
				} else {
					jQuery(element)
					.removeAttr('disabled')
					.closest('label')
					.removeClass('cb_disabled');
				}
			});
		};

		this.massSelectHandler = function (event) {
			event.preventDefault();
			var command = event.data.command;

			var all_filter = jQuery('input[type=checkbox][name^="param_filter["]', itti_self.containers.param_search);
			var all = jQuery('input[type=checkbox][name^="param_filter["]:not([disabled])', jQuery(event.target).closest('.param_filter_box'));
			var on = all.filter(":checked");
			var off = all.filter(":not(:checked)");

			// clear the range selectors
			itti_self.clearMinMaxSelectors(jQuery(event.target).closest('.param_filter_box'));

			switch (command) {
			case 'all':
				itti_self.cbSetChecked(all, true);
				break;
			case 'none':
				itti_self.cbSetChecked(all, false);
				break;
			case 'invert':
				itti_self.cbSetChecked(on, false);
				itti_self.cbSetChecked(off, true);
				break;

			case 'clearAll':

				// clear ALL the range selectors
				itti_self.clearMinMaxSelectors(itti_self.containers.param_search);

				itti_self.cbSetDisabled(all_filter, false);
				itti_self.cbSetChecked(all_filter, false);

				jQuery('select[name="manufacturer_id"]', itti_self.containers.containing_form).val(0);
				jQuery('select[name="package"]', itti_self.containers.containing_form).val('');
				jQuery('input[name="keywords"]', itti_self.containers.containing_form).val('').triggerHandler('change');
				break;
			}

			itti_self.startUpdatingResultCounters();
		};

		jQuery('a.cb_check_all', this.containers.param_search)
		.click({command:'all'}, this.massSelectHandler);

		jQuery('a.cb_check_clear', this.containers.param_search)
		.click({command:'none'}, this.massSelectHandler);

		jQuery('a.cb_check_invert', this.containers.param_search)
		.click({command:'invert'}, this.massSelectHandler);

		jQuery('.clear_param_filter', this.containers.main)
		.click({command:'clearAll'}, function(event){
			//itti_self.sync_listing = false;
			//itti_self.updateProductListing({content:''});
			itti_self.massSelectHandler(event);
		});

		// MIN/MAX LISTS

		this.onMinSelected = function (event) {
			var min = jQuery(event.target);
			var box = min.closest('.param_filter_box');
			var max = box.find('select.param_filter_max');
			var cbs = box.find('input[type=checkbox][name^="param_filter["]');
			var before_min = min.val()>0, after_max = false;

			if (min.val()=='0' && max.val()=='0') {
				itti_self.cbSetChecked(cbs.filter(':not([disabled])'), false);
			} else {
				cbs.each(function(index,element){
					if (min.val()>0 && jQuery(element).val()==min.val()) {
						before_min = false;
					}

					itti_self.cbSetChecked(jQuery(element).filter(':not([disabled])'), !before_min && !after_max);

					if (max.val()>0 && jQuery(element).val()==max.val()) {
						after_max = true;
					}
				});
			}

			var disable = (min.val() > 0);

			max.find('option[value!="0"]').each(function(index,element){
				if (disable)
					jQuery(element).attr('disabled', 'disabled');
				else
					jQuery(element).removeAttr('disabled');

				if (jQuery(element).val()==min.val())
					disable = false;
			});

			itti_self.startUpdatingResultCounters();
		};

		this.onMaxSelected = function (event) {
			var max = jQuery(event.target);
			var box = max.closest('.param_filter_box');
			var min = box.find('select.param_filter_min');
			var cbs = box.find('input[type=checkbox][name^="param_filter["]');
			var before_min = min.val()>0, after_max = false;

			if (min.val()=='0' && max.val()=='0') {
				itti_self.cbSetChecked(cbs.filter(':not([disabled])'), false);
			} else {
				cbs.each(function(index,element){
					if (min.val()>0 && jQuery(element).val()==min.val()) {
						before_min = false;
					}

					itti_self.cbSetChecked(jQuery(element).filter(':not([disabled])'), !before_min && !after_max);

					if (max.val()>0 && jQuery(element).val()==max.val()) {
						after_max = true;
					}
				});
			}

			var disable = false;

			min.find('option[value!="0"]').each(function(index,element){
				if (jQuery(element).val()==max.val())
					disable = true;

				if (disable)
					jQuery(element).attr('disabled', 'disabled');
				else
					jQuery(element).removeAttr('disabled');
			});

			itti_self.startUpdatingResultCounters();
		};

		this.clearMinMaxSelectors = function (container) {
			container.find('select.param_filter_min, select.param_filter_max').val('0')
				.find('option').removeAttr('disabled');
		};

		jQuery('select.param_filter_min', this.containers.main).change(this.onMinSelected);
		jQuery('select.param_filter_max', this.containers.main).change(this.onMaxSelected);

		// OVERLAY

		this.showSearchOverlay = function () {
			this.containers.param_search_overlay
			.css({
				top: '0px',
				left: '0px',
				right: '0px',
				bottom: '0px'
			}).show();
		};

		this.hideSearchOverlay = function () {
			this.containers.param_search_overlay.hide();
		};

		this.showProductOverlay = function () {
			var overlay = this.containers.product_listing_overlay.clone()
				.css({
					top: '0px',
					left: '0px',
					right: '0px',
					bottom: '0px'
				})
				.show();

			this.containers.product_listing.append(overlay);
			this.containers.empty_result.append(overlay.clone());
			this.containers.third_level_category_data.append(overlay.clone());
		};

		this.hideProductOverlay = function () {
			this.containers.product_listing.find('.loading_overlay').remove();
			this.containers.empty_result.find('.loading_overlay').remove();
			this.containers.third_level_category_data.find('.loading_overlay').remove();
		};


		// RESULT COUNT UPDATE

		this.abortAjax = function(){
			// abort running ajax requests
			if (this.ajax_result_counts!=undefined)  this.ajax_result_counts.abort();
			if (this.ajax_result_listing!=undefined) this.ajax_result_listing.abort();
		};

		this.updateResultCounters = function (data) {
			function getTime(){
				var d = new Date();
				return d.valueOf();
			}

			var t1 = getTime();

			var numbers = data.filter_count;
			var cb, label, counter;

			var process = function (count, cb, counter, label) {
				if (count > 0) {
					counter.innerHTML = '(' + count + ')';
					cb.disabled = false;
					$(label).removeClass('cb_disabled');
					if (cb.checked) {
						$(label).addClass('cb_checked');
					} else {
						$(label).removeClass('cb_checked');
					}
				} else {
					if (cb.checked) {
						cb.disabled = false;
						$(label).removeClass('cb_disabled');
					} else {
						counter.innerHTML = '';
						cb.disabled = true;
						$(label).addClass('cb_disabled');
					}
				}
			};

			for (var val in this.containers.checkboxes_by_id) {
				cb = this.containers.checkboxes_by_id[val];
				label = this.containers.labels_by_id[val];
				counter = this.containers.counters_by_id[val];
				process(numbers[val], cb, counter, label);
			}

			if (this.sync_listing) {
				if (data['listing']==undefined) {
					this.startUpdatingProductListing();
				} else {
					this.updateProductListing(data['listing']);
				}
			}

			this.hideSearchOverlay();
			this.hideProductOverlay();
		};

		this.startUpdatingResultCounters = function () {
			this.showSearchOverlay();

			if (this.sync_listing) this.showProductOverlay();

			var form_data = jQuery('input, textarea, select', this.containers.main).serializeArray();
			form_data.push({name:'sync_listing',value:this.sync_listing ? 1 : 0});
			form_data.push({name:'log_stats',value:this.log_stats ? 1 : 0});

			this.abortAjax();

			this.ajax_result_counts = jQuery.post('/ajax/get_result_counts.php', form_data, function(data){
				itti_self.updateResultCounters(data);
			}, 'json');
		};


		this.createPagebarButtons = function (current, total) {
			current = Number(current);

			var leftmost = current - 3;
			var rightmost = current + 3;
			var prev = Math.max(current - 1, 1);
			var next = Math.min(current + 1, total);
			var shift = 0;

			if (leftmost < 1) {
				rightmost += 1 - leftmost;
				leftmost = 1;
				if (rightmost > total) rightmost = total;
			}

			if (rightmost > total) {
				leftmost -= rightmost - total;
				rightmost = total;
				if (leftmost < 1) leftmost = 1;
			}

			this.containers.pagebar.children(':not(input[type="hidden"])').remove();

			if (total <= 1) return;

			if (total > 2) {
				this.containers.pagebar.append(
					jQuery('<a href="#" class="first">«</a>')
						.click({page: 1}, this.setPageHandler)
				);
			}

			this.containers.pagebar.append(
				jQuery('<a href="#" class="prev">‹</a>')
					.click({page:prev}, this.setPageHandler)
			);

			this.containers.pagebar.append(
				jQuery('<span class="indicator">'+current+' / '+total+'</span>')
			);

			this.containers.pagebar.append(
				jQuery('<a href="#" class="next">›</a>')
					.click({page:next}, this.setPageHandler)
			);

			if (total > 2) {
				this.containers.pagebar.append(
					jQuery('<a href="#" class="last">»</a>')
						.click({page: total}, this.setPageHandler)
				);
			}

			if (current === 1) {
				this.containers.pagebar.find('>.first, >.prev').addClass('disabled');
			}

			if (current === total) {
				this.containers.pagebar.find('>.last, >.next').addClass('disabled');
			}
		};


		this.createOnePagebarButton = function (page, current) {
			var result;

			// the distance is added as attribute for styling (e.g. hide larger distances for mobile)
			var distance = Math.abs(page - current);

			if (page===current) {
				result = jQuery('<a href="#" class="current">'+page+'</a>');
			} else {
				result = jQuery('<a href="#">'+page+'</a>');
			}

			result.attr('data-page-distance', distance);

			result.click({page:page}, this.setPageHandler);

			return result;
		};


		this.setPageHandler = function (event) {
			event.preventDefault();
			jQuery('input[name="page"]', itti_self.containers.main).val(event.data.page);
			itti_self.startUpdatingProductListing();
		};


		this.updateProductListing = function (data) {

			if (this.containers.category_listing.size()>0) {
				if (data.categories) {
					this.containers.category_listing.closest('.main_content_container').show();
					this.containers.category_listing.get(0).innerHTML = data.categories;
				} else {
					this.containers.category_listing.closest('.main_content_container').hide();
				}
			}

			if (data.total==0 && this.containers.empty_result.size()>0 && data['empty_result']!=undefined) {
				this.containers.product_listing.hide();
				jQuery('.search_result_bar', this.containers.main).hide();
				this.containers.empty_result.get(0).innerHTML = data['empty_result'];
				this.containers.empty_result.show();
				this.containers.request_form.show().find('input[name="product"]').val(jQuery('input[name="keywords"]', this.containers.containing_form).val());

				this.hideProductOverlay();
				this.saveToURL();
				return;
			}

			if (this.containers.third_level_category_data.size()>0 && data['third_level_category_data']!=undefined) {
				this.containers.third_level_category_data.get(0).innerHTML = data['third_level_category_data'];
			}

			this.containers.product_listing.show();
			jQuery('.search_result_bar', this.containers.main).show();
			this.containers.empty_result.hide();
			this.containers.request_form.hide();

			this.containers.product_listing.get(0).innerHTML = data.content;
			window.shoppingCart.initContent(this.containers.product_listing.get(0));

			jQuery('td:first-child', this.containers.product_listing).addClass('first_col');
			jQuery('td:last-child', this.containers.product_listing).addClass('last_col');

			// update sorting classes and attach sort actions
			var current_sort = jQuery('input[name="sort"]', itti_self.containers.main).val();
			var find_class = current_sort.charAt(0)=='+' ? 'sort_asc' : 'sort_desc';
			current_sort = current_sort.substr(1);

			if (current_sort!='') {
				jQuery('.sortkey', this.containers.product_listing).each(function(){
					if (this.innerHTML==current_sort) {
						jQuery(this).closest('td').addClass('sorted').find('a.'+find_class).addClass('sorted');
					}
				});

//				jQuery('.sortkey:contains("'+current_sort+'")', this.containers.product_listing).first()
//					.closest('td').addClass('sorted')
//					.find('a.'+find_class).addClass('sorted');
			}

			jQuery('a.sort_asc,a.sort_desc', this.containers.product_listing).each(function(index, element){
				var sort_key = jQuery(element).closest('td').find('.sortkey').text();

				if (jQuery(element).hasClass('sort_asc')) {
					sort_key = '+'+sort_key;
				} else {
					sort_key = '-'+sort_key;
				}

				jQuery(element).click({sort_key:sort_key}, function(event){
					event.preventDefault();

					if (sort_key == jQuery('input[name="sort"]', itti_self.containers.main).val()) {
						jQuery('input[name="sort"]', itti_self.containers.main).val('');
					} else {
						jQuery('input[name="sort"]', itti_self.containers.main).val(sort_key);
					}

					itti_self.startUpdatingProductListing();
				});
			});

			jQuery('input[name="page"]', this.containers.main).val(data.page);
			this.createPagebarButtons(data.page, data.pages);

			try {
				jQuery('.search_result_count_indicator', this.containers.main).get(0).innerHTML = data.total;
			} catch (e) {}

			this.comparison.updateCheckboxes.call(this);
			this.comparison.updateCounter.call(this);
			this.comparison.restoreClickHandlers.call(this);

			this.hideProductOverlay();
			this.saveToURL();

			requestAnimationFrame(function(){
				var elem = this.containers.product_listing.closest('.search_result_list');
				// floating thead
				this.containers.product_listing.find('>table.search_result_list').floatThead({"autoReflow":true,"position":"absolute"});
				// update handy resize
				if (typeof handyScroll !== 'undefined') {
					if (handyScroll.mounted(elem[0])) {
						handyScroll.update(elem[0]);
					} else {
						handyScroll.mount(elem[0]);
						elem.on('scroll', function(){
							this.containers.product_listing.find('>table.search_result_list').floatThead('reflow');
						}.bind(this));
					}
				}
			}.bind(this));

			// fire up farnell price loader
			jQuery(window).triggerHandler('resize');
		};


		this.startUpdatingProductListing = function () {
			var form_data = jQuery('input, textarea, select', this.containers.main).serializeArray();
			this.showProductOverlay();

			form_data.push({name:'log_stats',value:this.log_stats ? 1 : 0});

			this.abortAjax();

			this.ajax_result_listing = jQuery.post('/ajax/get_result_listing.php', form_data, function(data){
				itti_self.updateProductListing(data.listing);
			}, 'json');
		};


		jQuery('a.submit_param_filter', this.containers.main).click(function(event){
			event.preventDefault();
			itti_self.sync_listing = true;
			itti_self.startUpdatingProductListing();
		});

		jQuery('a.compare_button,button.compare_button', this.containers.main).click(function(event){
			event.preventDefault();
			if (!itti_self.comparison.doCompare.call(itti_self)) {
				alert(window.translation.product_comparison_needs_2);
			}
		});

		// update result counters on document load
		jQuery(function(){

			// doesn't work in the constructor
			jQuery('select[name="items_per_page"]', itti_self.containers.main)
			.add('select[name="currency"]', itti_self.containers.main)
			.change(function(event){
				if (itti_self.sync_listing)
					itti_self.startUpdatingProductListing();
			});

			jQuery('select[name="manufacturer_id"]', itti_self.containers.containing_form).change(function(event){
				itti_self.startUpdatingResultCounters();
			});

			jQuery('select[name="package"]', itti_self.containers.containing_form).change(function(event){
				itti_self.startUpdatingResultCounters();
			});

			jQuery('.submit_keywords', itti_self.containers.containing_form).click(function(event){
				event.preventDefault();
				itti_self.startUpdatingResultCounters();
			});

			jQuery('input[name="keywords"]', itti_self.containers.containing_form).keypress(function(event){
				if (event.which != 13) return true;
				$(this).triggerHandler('change');
				event.preventDefault();
				itti_self.startUpdatingResultCounters();
				return false;
			});

			itti_self.loadFromURL();
			itti_self.startUpdatingResultCounters();
		});

		this.saveToURL = function () {
			var result = {c:[],s:'',p:1};
			for (var id in this.containers.checkboxes_by_id) {
				if (this.containers.checkboxes_by_id[id].checked)
					result.c.push(this.containers.checkboxes_by_id[id].value);
			}

			result.s = jQuery('input[name="sort"]', this.containers.main).val();
			result.p = jQuery('input[name="page"]', this.containers.main).val();
			result.ipp = jQuery('select[name="items_per_page"]', this.containers.main).val();
			result.m = jQuery('select[name="manufacturer_id"]', this.containers.containing_form).val();
			result.pk = jQuery('select[name="package"]', this.containers.containing_form).val();
			result.k = jQuery('input[name="keywords"]', this.containers.containing_form).val();
			is_new = jQuery('input[name="is_new"]', this.containers.containing_form).val();

			if (result.c.length==0)    delete result.c;
			if (result.s=='')          delete result.s;
			if (parseInt(result.p)==1) delete result.p;
			if (parseInt(result.m)==0) delete result.m;
			if (result.pk=='')         delete result.pk;
			if (result.k=='')          delete result.k;

			var l = window.location;
			var searchnew=l.search;
//*	

			if (parseInt(is_new)) {
			if (parseInt(result.m)>0) {

				vypros = l.href.indexOf('?')<0 ? '?':'&';
				if (l.href.indexOf('mc') < 0) {
					searchnew=l.search+vypros+'mc='+parseInt(result.m);
				} else {
					var searchString = l.search.substring(1),
					i, val, params = searchString.split("&");


					for (i=0;i<params.length;i++) {
						val = params[i].split("=");
						if (val[0] == 'mc') {
							mcvalue= val[1];
						}
					}
					if (mcvalue!=parseInt(result.m)) {
						var staro="mc="+mcvalue;
						var novo="mc="+parseInt(result.m);
						searchnew=l.search.replace(staro,novo);
					}
				}
			} else {
				if (l.href.indexOf('mc') >0) {
					if (l.href.indexOf('?mc')>0) {
						var searchString = l.search.substring(1),
						i, val, params = searchString.split("=");
						mcvalue=params[1];
					} else {
						searchString = l.search.substring(1),
						i, val, params = searchString.split("&");
						for (i=0;i<params.length;i++) {
						val = params[i].split("=");
						if (val[0] == 'mc') {
							mcvalue= val[1];
						}
						}
					}
					if (mcvalue>0) {
						if (l.href.indexOf('?mc')>0) {
							staro="?mc="+mcvalue;
						} else {
							staro="&mc="+mcvalue;
						}
						novo="";
						searchnew=l.search.replace(staro,novo);
					}
				} else {
					searchnew=l.search;
				}
			}
			}
//*/

			// location.replace avoids putting history entries in the browser
			window.location.replace(l.protocol+'//'+l.host+l.pathname+searchnew+'#'
				+ encodeURIComponent(ITTI.pack(result)));
		};

		this.loadFromURL = function () {
			if (window.location.hash.length<2) return 0;

			this.sync_listing = true;

			try {
				var hash = ITTI.unpack(decodeURIComponent(window.location.hash.substr(1)));
			} catch (err) {
				return 0;
			}

			for (var key in hash) {
				switch (key) {
				case 'c':
					for (var j in hash[key]) {
						this.cbSetChecked(jQuery(this.containers.checkboxes_by_id[hash[key][j]]), true);
					}
					break;

				case 's':
					jQuery('input[name="sort"]', this.containers.main).val(hash[key]);
					break;

				case 'p':
					jQuery('input[name="page"]', this.containers.main).val(hash[key]);
					break;

				case 'ipp':
					jQuery('select[name="items_per_page"]', this.containers.main).val(hash[key]);
					break;

				case 'm':
					jQuery('select[name="manufacturer_id"]', this.containers.containing_form).val(hash[key]);
					break;

				case 'pk':
					jQuery('select[name="package"]', this.containers.containing_form).val(hash[key]);
					break;

				case 'k':
					jQuery('input[name="keywords"]', this.containers.containing_form).val(hash[key]).triggerHandler('change');
					break;
				}
			}
		};

	}, // searchform END

	shoppingCart : function () {

		// you can use "itti_self" in inline functions defined in the current scope
		var itti_self = this;
		this.items = {};

		this.add = function (product_id, quantity, sourceElement, onSuccess) {
			jQuery.post(
				'/ajax/shopping_cart.php',
				{product_id:product_id,quantity:quantity},
				function (data) {
					itti_self.updateCartIndicators(data);

					if (data.error!=undefined && data.error!='') {
						alert(window.translation[data.error]);
					} else {
						if (typeof(onSuccess)=='function') onSuccess();
						else if (sourceElement) sourceElement.flyTo(jQuery('.shoppingcart').get(0));
					}
				},
				'json');
		};

		this.remove = function (product_id) {
			this.add(product_id, 0, null);
		}

		this.refresh = function () {
			this.add(0, 0, null);
		};

		this.refreshCartIndicators = function(){
			jQuery.post(
				'/ajax/shopping_cart.php',
				{},
				function (data) {
					itti_self.updateCartIndicators(data);
				},
				'json');
		};

		// call this to refresh the header button and all product order indicators throughout the page
		this.updateCartIndicators = function (data) {
			if (typeof data["items"] !== 'object') {
				return;
			}

			if (data.items === null) {
				return;
			}

			this.items = data.items;

			jQuery('#cart_items').get(0).innerHTML = Object.keys(data.items).length;
			// jQuery('#cart_total').get(0).innerHTML = data.total_price;

			jQuery('.order_btn,.order_btn_horiz').each(function(index,element){
				// product_id may be prefixed with "farnell:", so it's not an integer
				var product_id = jQuery(element).find('input.order_product_id').val();

				try {
					var quantity = data.items[product_id]['quantity'];
				} catch (e) {
					var quantity = 0;
				}

				if (quantity > 0) {
					jQuery(element).find('input.order_quantity').val(quantity);
				} else {
					jQuery(element).find('input.order_quantity').val('');
				}
			});

		};

		this.customCodeSelect = function (product_id) {
			jQuery.post(
				'/ajax/shopping_cart.php',
				{custom_code_select:product_id},
				function (data) {
					window.location.href = window.shopping_cart_url;
				},
				'json');
		};

		// initializes an HTML element (attaches handlers, updates quantity inputs, etc.)
		// used when injecting content retrieved through ajax
		this.initContent = function (element) {
			jQuery('.order_btn,.order_btn_horiz', element).each(function(index,element){
				var product_id = jQuery(element).find('input.order_product_id').val();

				// attach button handlers
				jQuery(this).find('a,button').click(function(event){
					event.preventDefault();
					var q = jQuery(element).find('input.order_quantity').val();

					if (q=='') q = '1';

					if (window.logged_in) {
						if (jQuery(element).hasClass('goback')) {
							var on_success = function(){
								window.location.href = window.shopping_cart_url;
							};
						} else {
							var on_success = null;
						}

						itti_self.add(product_id, q, jQuery(this).closest('.order_btn,.order_btn_horiz'), on_success);
					} else {
						ITTI.loginRedirect('product_id='+product_id+'&quantity='+q);
					}

					return false;
				});

				jQuery(element).find('input.order_quantity').keypress(function(event){
					if (event.which != 13) return true;

					event.preventDefault();

					// if "enter" is pressed, simulate click on the button
					jQuery(element).find('a').click();

					return false;
				});

			});

			this.refresh();
		};

	},

	loginRedirect : function (extra_params) {
		// decodeURIComponent is used to save some space
		var return_to = base64_encode(decodeURIComponent(
			window.location.pathname + window.location.search + window.location.hash));
		window.location = window.login_page_url + '?r=' + return_to + (extra_params!='' ? '&'+extra_params : '');
	},

	collapsePanel : {
		onClickHandler : function (source) {
			jQuery(source).closest('.panel-collapse').toggleClass('opened');
			$(window).triggerHandler('resize');
		}
	}, // collapsePanel END

	sessionSettings : {
		get : function (setting_name) {
			return Cookies.get('settings_'+setting_name);
		},
		set : function (setting_name, value, cookie_options) {
			cookie_options = jQuery.extend({
				expires:30,
				path:'/',
			}, cookie_options);
			for (let k in cookie_options) {
				if (cookie_options[k]===null) delete cookie_options[k];
			}
			Cookies.set('settings_'+setting_name, value, cookie_options);
		}
	} // sessionSettings END

};
