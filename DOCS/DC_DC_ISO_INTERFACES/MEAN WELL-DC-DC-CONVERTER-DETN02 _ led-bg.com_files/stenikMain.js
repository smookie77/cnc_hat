﻿jQuery(function($) {
	$('nav.mainNav ul li.hasSub').hover(function(){
		$('.mainNavSub', this).slideDown();
	},function(){
		$('.mainNavSub', this).hide();
	});

	$('.productListingTable .phone').hover(function(){
		$('.tooltipText', this).slideDown();
	},function(){
		$('.tooltipText', this).hide();
	});

	$('.rightColForm input.error').click(function(){
		$(this).removeClass('error');
	});
	
	$('.rightColForm textarea.error').click(function(){
		$(this).removeClass('error');
	});


	// Responsive js
	stenikResponsiveMenu();


	stenikHorizontalScroll('.responsiveScrollWrapper', 30, 50);
});

/* Table responsive horizontal scroll wrapper
******************************************************************* */
function stenikHorizontalScroll(selector, step, hold_interval){
	if(!step) step = 40;
	if(!hold_interval) hold_interval = 50;

	

	var $selector = jQuery(selector);
	// Generate the markup
	$selector.wrapInner('<div class="scrollWrapper"><div class="scrollWrapperInner"></div></div>').append('<span class="scrollHandle prev"></span><span class="scrollHandle next"></span>');

	var nua = navigator.userAgent;
	var is_old_android = ((nua.indexOf('Mozilla/5.0') > -1 && nua.indexOf('Android') > -1 && nua.indexOf('AppleWebKit') > -1) && !(nua.indexOf('Chrome') > -1));
	if(is_old_android) {
		// --- disable native scroll for old stock android browser
		$selector.addClass('disableScroll');

		// --- enable only arrows click navigation
		$selector.find('.scrollHandle.next').click(function(){
			scrollAction(this, 'forward');
		});
		$selector.find('.scrollHandle.prev').click(function(){
			scrollAction(this, 'backward');
		});
	} else {
		// --- mouse/touch hold support
		var timer = null;
		$selector.find('.scrollHandle.next').on('mousedown touchstart', function(){
			var $that = jQuery(this); 
			clearInterval(timer);
			timer = setInterval(function(){
				scrollAction($that, 'forward');
			}, hold_interval);
		});
		$selector.find('.scrollHandle.prev').on('mousedown touchstart', function(){
			var $that = jQuery(this); 
			clearInterval(timer);
			timer = setInterval(function(){
				scrollAction($that, 'backward');
			}, hold_interval);
		});

		// --- resets
		$selector.find('.scrollHandle.next').on('mouseup touchend touchcancel', function(){
			clearInterval(timer);
		});
		$selector.find('.scrollHandle.prev').on('mouseup touchend touchcancel', function(){
			clearInterval(timer);
		});

		// --- clear handle classes on native scroll
		$selector.find('.scrollWrapper').scroll(function(){
			scrollAction(this);
		});
	}

	function scrollAction(item, direction) { // direction? 'forward' : 'backward' 
		var $parent = jQuery(item).parents(selector).first();
		var $scrollWrapper = $parent.find('.scrollWrapper');
		var $scrollWrapperInner = $parent.find('.scrollWrapperInner');
		var currentScrollPosition = $scrollWrapper.scrollLeft();

		if(direction === 'forward') {
			$scrollWrapper.scrollLeft(currentScrollPosition + step);
		} else if(direction === 'backward') {
			$scrollWrapper.scrollLeft(currentScrollPosition - step);
		}

		if($scrollWrapper.scrollLeft() == 0) {
			$parent.addClass('startReached').removeClass('endReached');
			clearInterval(timer);
		}
		else if($scrollWrapperInner.width() - $scrollWrapper.scrollLeft() == $scrollWrapper.width()) {
			$parent.addClass('endReached').removeClass('startReached');
			clearInterval(timer);
		}
		else {
			$parent.removeClass('startReached').removeClass('endReached');
		}
	}
}

/* Responsive side panel navigation
******************************************************************* */
function stenikResponsiveMenu(){
	var $pageWrapper = jQuery('html');
	var $headerWrapper = jQuery('#header .wrapper');

	// Create the markup
	$headerWrapper.append(
		'<div id="menu_handle" class="menuHandle"><span class="iconHelper"></span></div>'+
		'<nav id="responsive_menu" class="responsiveMenu"></nav>'
	);
	var $handle = jQuery('#menu_handle');
	var $responsiveMenu = $('#responsive_menu');

	$responsiveMenu.append(jQuery('#header_top_nav').html() + jQuery('#main_nav').html());

	$hasChildLinks = $responsiveMenu.find('li.hasSub').children('a');

	// // Disable hasChild link click and toggle responsive dropdown
	$hasChildLinks.click(function(e){
		e.preventDefault();
		$parent = jQuery(this).parent();
		if($parent.hasClass('open-click')) {
			$parent.removeClass('open-click');
		} else {
			$parent.addClass('open-click');
		}
	});

	$handle.click(function(e){
		e.preventDefault();
		toggleHelper();
	});
	jQuery(document).on('mouseup touchend', function(e) {
		if($responsiveMenu.is(e.target)) {
			toggleHelper('hide');
		}
	});

	function toggleHelper(action) {
		if($handle.hasClass('active') || action === 'hide') {
			$pageWrapper.removeClass('panelOpen');
			$handle.removeClass('active');
		} else if (!$handle.hasClass('active') || action === 'show'){
			$pageWrapper.addClass('panelOpen');
			$handle.addClass('active');
			$('#responsive_menu').on('scroll touchmove', function(e) {
				e.stopPropagation();
			});
		}
		// --- reset dropdown
		$hasChildLinks.parent().removeClass('open-click');
	}
}
